'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// db config
const mongoose = require('mongoose');
const mongoUrl = "mongodb://db:27017/test";
const dbOptions = {};

// db event callbacks
var db = mongoose.connection;
db.on('error', function() {
	console.log('Error connecting to database.');
});
db.once('open', function() {
  console.log('Connection to database established!');
});

// connect to DB
var setupConnection = function() {
	return mongoose.connect(mongoUrl, dbOptions, function(err) {
		if (err) {
			console.error('Could not connect to database on startup, retrying in 3 seconds.');
			setTimeout(setupConnection, 3000);
		}
	});
}
setupConnection();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('views', './views');
app.set('view engine', 'ejs');
app.use('/css', express.static('./public/css'));
app.use('/js', express.static('./public/js'));

// controllers
const timetableController = require('./controller/timetable');

// routes
app.get('/', function (req, res) {
	timetableController.get(function(data) {
		res.render('index', {
			data: data
		});
	});
});
app.get('/data', function (req, res) {
	timetableController.get(function(data) {
		res.status(200).json(data);
	});
});

// start server
app.listen(3000, function () {
	console.log('Demo app listening on port 3000!');
});
