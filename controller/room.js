var room = {
	id: 1,
	location: {
		room: 'Stoddart-7139',
		area: 'City'
	},
	capacity: {
		current: 30,
		total: 30
	}
};

// returns the room details
exports.getRoomDetails = function() {
	return room;
}

// fetch the current room capacity
exports.getCapacity = function() {
	var capacity = room.capacity.current;
	return capacity;
}

// set the current room capacity
exports.setCapacity = function(capacity) {
	room.capacity.current = capacity;
	return;
}

// calculate the current room capacity after demand
exports.calculateCapacity = function(demand) {
	var currentCapacity = room.capacity.current;
	var capacity = currentCapacity - demand;
	return capacity;
}

// add a student to the room, reducing current capacity
exports.addStudent = function() {
	room.capacity.current + 1;
	return;
}

// remove a student from the room, increasing current capacity
exports.addStudent = function() {
	room.capacity.current - 1;
	return;
}