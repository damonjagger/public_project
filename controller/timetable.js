exports.get = function(callback) {

	const data = {
		monday: {
			'12:00': {
				title: 'Mobile Security',
				type: 'Lecture',
				tutor: 'Richardson, Neil',
				location: {
					room: 'Cantor-9140',
					area: 'City'
				}
			},
			'14:00': {
				title: 'Enterprise Applications Management',
				type: 'Lecture',
				tutor: 'Wheway, Paul; Douglas, Roderick; Hughes, Iain',
				location: {
					room: 'Cantor-9132',
					area: 'City'
				}
			},
			'15:00': {
				title: 'Web Security',
				type: 'Lecture',
				tutor: 'Richardson, Neil',
				location: {
					room: 'Stoddart-7139',
					area: 'City'
				}
			},
			'16:00': {
				title: 'Information Security Management',
				type: 'Lecture',
				tutor: 'Ibbotson, Gregg',
				location: {
					room: 'Cantor-9005',
					area: 'City'
				}
			},
		},
		tuesday: {},
		wednesday: {},
		thursday: {},
		friday: {
			'11:00': {
				title: 'Enterprise Applications Management',
				type: 'It Session',
				tutor: 'Unstaffed',
				location: {
					room: 'Cantor-9306',
					area: 'City'
				}
			},
		}
	};

	callback(data);
}