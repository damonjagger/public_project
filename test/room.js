var assert = require('assert');
var should = require('should');
var room = require('../controller/room');
var roomStub = require('./stub/room').room;

describe('Room controller', function() {

	describe('.getRoomDetails', function() {
		it('should return a room details object', function() {
			var roomDetails = room.getRoomDetails();
			should.exist(roomDetails);
		});

		it('room details object should match test stub', function() {
			var roomDetails = room.getRoomDetails();
			roomDetails.should.deepEqual(roomStub);
		});
	});

	describe('.getCapacity', function() {
		it('should return a valid number between 0 and 30 (max capacity)', function() {
			var capacity = room.getCapacity();
			should.exist(capacity);
			(capacity).should.be.a.Number;
			(capacity).should.not.be.NaN();
			(capacity).should.be.aboveOrEqual(0);
			(capacity).should.be.belowOrEqual(30);
		});

		it('capacity should match test stub', function() {
			var capacity = room.getCapacity();
			var stubCapacity = roomStub.capacity.current;
			capacity.should.deepEqual(stubCapacity);
		});
	});

	describe('.calculateCapacity', function() {
		it('should return a valid number between 0 and 30 (max capacity)', function() {
			var capacity = room.calculateCapacity(1);
			should.exist(capacity);
			(capacity).should.be.a.Number;
			(capacity).should.not.be.NaN();
			(capacity).should.be.aboveOrEqual(0);
			(capacity).should.be.belowOrEqual(30);
		});

		it('should return the capacity minus the demand', function() {
			var capacity = room.getCapacity();
			var calculatedCapacity = room.calculateCapacity(5);
			var targetCapacity = capacity - 5;
			calculatedCapacity.should.equal(targetCapacity);
		});
	});


exports.calculateCapacity = function(demand) {
	var currentCapacity = room.capacity.current;
	var capacity = currentCapacity - demand;
	return capacity;
}

});
